import sys
import numpy as np
import csv
import os as os
import pandas as pd
import re
import json
import matplotlib.pyplot as plt
from datetime import date, datetime
from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtWidgets import QMessageBox
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure
from bitarray import bitarray

# My functions
from Libraries import cal_IO
#from DBLR_lab import find_baseline
from Libraries import fit_library as fit
from scipy.optimize import curve_fit
from Libraries import ASIC_param

qtCreatorFile = "PETALO_v3.ui" # Enter file here.

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

class LAB_data():
    """ Main data configuration for the program.
        Method

        Parameters

    """
    fig1 = Figure()
    # fig2 = Figure()
    # fig3 = Figure()
    # fig4 = Figure()

    axes={'ax1':0,'ax2':0,'ax3':0,'ax4':0, 'ax5':0}
    # Axes 1 & 2 for figure 1. Axes 3 for figure 2

    #baseline_txt = fig1.text(0.6,0.8, ('BASELINE = '))
    #mu_txt = fig2.text(0.15,0.8, ('MU = '))
    #sigma_txt = fig2.text(0.15,0.75, ('SIGMA = '))
    #res_txt = fig2.text(0.15,0.7, ('RES(fwhm) = '))
    #lin_txt = fig3.text(0.15,0.7, ('CHI2_r = '))
    #lin2_txt = fig3.text(0,0.91, ('Photons'))
    #lin3_txt = fig3.text(0.8,0.02, ('Pulse (us)'))
    #mu_txt_spe = fig4.text(0.15,0.8, ('MUs = '))
    #mu_txt_spe2 = fig4.text(0,0.91, ('Hits'))
    #mu_txt_spe3 = fig4.text(0.8,0.02, ('Counts (25ns)'))
    #sigma_txt_spe = fig4.text(0.15,0.75, ('SIGMAs = '))


    Config_data = {'time_r':5, 'events':250, 'f_size':100}


    site = "/Users/vins/Documents/IFIC/PETALO/PYTHON/DATA/box0_ch0_50u.h5"
    site2 = "/Users/vins/Documents/IFIC/PETALO/PYTHON/Configurations/Config_test.json"

    files = {'report_file':"CONF.csv",
          'conf_name':"Config_test",
          'dir':site + ""}

    #df_config=pd.DataFrame(columns=['BUFFER_SIZE','PRETRIGGER','N_TRIGGERS',' '])

    Pass = ""

    flags = {'all_access':False, 'shifter_access':False}


class CORE():
    """ Core of the program.
            Method

            Parameters

    """
    def __init__(self,upper_class):
        self.uc = upper_class

    def f_PLOT(self):

        f = cal_IO.read_DATE_hdf5(self.uc.d.site,
                                    200,
                                    9)
        bins=50

        hist, bin_edges = np.histogram(f, bins=bins)
        bin_centres = (bin_edges[:-1] + bin_edges[1:])/2

        p0 = [1, np.mean(f), np.std(f)]
        coeff, var_matrix = curve_fit(fit.gauss, bin_centres, hist, p0=p0)

        #Gets fitted function and residues
        hist_fit = fit.gauss(bin_centres, coeff[0],coeff[1],coeff[2])

        #Parameters error estimation (sigma). See numpty user guide
        perr = np.sqrt(np.diag(var_matrix))


        print(f)

        #mu, sigma = 100, 15
        #x = [1,11,14,15,20,34,50]
        #bins = []

        #hist, bins = np.histogram(x, bins=bins)
        #width = np.diff(bins)
        #center = (bins[:-1] + bins[1:]) / 2

        self.uc.d.axes['ax1'].hist(f, bins=bins)

        self.uc.d.axes['ax1'].set_xlabel("Samples")
        self.uc.d.axes['ax1'].set_ylabel("Bins")

        self.uc.d.axes['ax1'].plot(0, 0, 'r--', linewidth=1)

        self.uc.canvas1.draw()

    def Start_run(self):

       self.uc.textBrowser.append("The run has start")
       self.uc.Log.setText("The run is ongoing")
       self.uc.textBrowser.append(str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"\n")
       print ("The run has start")

    def Stop_run(self):

       self.uc.textBrowser.append("The run is stopped")
       self.uc.Log.setText("The run is stopped")
       self.uc.textBrowser.append(str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"\n")
       print ("The run is stopped")

    def Config_reg(self):

       self.uc.textBrowser.append("Registers are configured ("+self.uc.lineEdit_conf_file.text()+")")
       self.uc.Log.setText("Registers are configured")
       self.uc.textBrowser.append(str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"\n")
       print ("Registers are configured")

    def Config_reg_glob_ASIC(self):

       self.uc.textBrowser.append("Global ASIC registers configured")
       self.uc.Log.setText("Global ASIC registers configured")
       self.uc.textBrowser.append(str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"\n")
       print ("Global ASIC registers configured")

    def Config_reg_ch_ASIC(self):

       self.uc.textBrowser.append("Channel ASIC registers configured")
       self.uc.Log.setText("Channel ASIC registers configured")
       self.uc.textBrowser.append(str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"\n")
       print ("Channel ASIC registers configured")

    def validate_pass(self):

        self.uc.Pass = self.uc.pass_lineEdit.text()

        if self.uc.Pass == "Petalo":
           self.uc.Log.setText('Superuser access')
           self.uc.textBrowser.append("Access as superuser")
           self.uc.textBrowser.append(str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"\n")
           self.uc.d.flags['all_access'] = True
           self.uc.d.flags['shifter_access'] = False

           self.uc.SpinBox_Buffer.setEnabled (True)
           self.uc.SpinBox_Pretrigger.setEnabled (True)
           self.uc.SpinBox_Triggers.setEnabled (True)

           self.uc.comboBox_tx_nlinks.setEnabled (True)
           self.uc.comboBox_tx_ddr.setEnabled (True)
           self.uc.comboBox_tx_mode.setEnabled (True)
           self.uc.checkBox_debug_mode.setEnabled (True)
           self.uc.comboBox_veto_mode.setEnabled (True)
           self.uc.comboBox_tdk_clk_div.setEnabled (True)
           self.uc.checkBox_r_clk_en.setEnabled (True)
           self.uc.checkBox_stop_ramp_en.setEnabled (True)
           self.uc.checkBox_counter_en.setEnabled (True)
           self.uc.comboBox_counter_period.setEnabled (True)
           self.uc.checkBox_tac_refresh_en.setEnabled (True)
           self.uc.comboBox_tac_refresh_period.setEnabled (True)
           self.uc.comboBox_data_clk_div.setEnabled (True)
           self.uc.checkBox_fetp_enable.setEnabled (True)
           self.uc.comboBox_input_polarity.setEnabled (True)
           self.uc.comboBox_attenuator_ls.setEnabled (True)
           self.uc.comboBox_v_ref_diff_bias_ig.setEnabled (True)
           self.uc.comboBox_v_cal_ref_ig.setEnabled (True)
           self.uc.comboBox_fe_postcamp_t.setEnabled (True)
           self.uc.comboBox_fe_postcamp_e.setEnabled (True)
           self.uc.comboBox_v_cal_tp_top.setEnabled (True)
           self.uc.comboBox_v_cal_diff_bias_ig.setEnabled (True)
           self.uc.comboBox_v_att_diff_bias_ig.setEnabled (True)
           self.uc.comboBox_v_integ_ref_ig.setEnabled (True)
           self.uc.comboBox_imirror_bias_top.setEnabled (True)
           self.uc.comboBox_tdc_comp_bias.setEnabled (True)
           self.uc.comboBox_tdc_i_lsb.setEnabled (True)
           self.uc.comboBox_disc_lsb_t1.setEnabled (True)
           self.uc.comboBox_fe_ib2.setEnabled (True)
           self.uc.comboBox_vdiffoldcas.setEnabled (True)
           self.uc.comboBox_disc_vcas.setEnabled (True)
           self.uc.comboBox_disc_lsb_e.setEnabled (True)
           self.uc.comboBox_tdc_i_ref.setEnabled (True)
           self.uc.comboBox_tdc_comp_vcas.setEnabled (True)
           self.uc.comboBox_fe_ib2_x2.setEnabled (True)
           self.uc.comboBox_main_global_dac.setEnabled (True)
           self.uc.comboBox_fe_ib1.setEnabled (True)
           self.uc.comboBox_disc_ib.setEnabled (True)
           self.uc.comboBox_disc_lsb_t2.setEnabled (True)
           self.uc.comboBox_tdc_tac_vcas_p.setEnabled (True)
           self.uc.comboBox_tdc_tac_vcas_n.setEnabled (True)
           self.uc.comboBox_adebug_out_mode.setEnabled (True)
           self.uc.comboBox_tdc_global_dac.setEnabled (True)
           self.uc.comboBox_adebug_buffer.setEnabled (True)
           self.uc.comboBox_disc_sf_bias.setEnabled (True)

           self.uc.comboBox_set_trigger_mode1.setEnabled (True)
           self.uc.comboBox_debug_mode.setEnabled (True)
           self.uc.comboBox_sync_chain_lenght.setEnabled (True)
           self.uc.comboBox_dead_time.setEnabled (True)
           self.uc.comboBox_counter_mode.setEnabled (True)
           self.uc.comboBox_tac_max_age.setEnabled (True)
           self.uc.comboBox_tac_min_age.setEnabled (True)
           self.uc.comboBox_set_trigger_mode2_t.setEnabled (True)
           self.uc.comboBox_set_trigger_mode2_e.setEnabled (True)
           self.uc.comboBox_set_trigger_mode2_q.setEnabled (True)
           self.uc.comboBox_set_trigger_mode2_b.setEnabled (True)
           self.uc.comboBox_branch_en_eq.setEnabled (True)
           self.uc.comboBox_branch_en_t.setEnabled (True)
           self.uc.comboBox_qdc_mode.setEnabled (True)
           self.uc.comboBox_set_trigger_b_latched.setEnabled (True)
           self.uc.comboBox_min_intg_time.setEnabled (True)
           self.uc.comboBox_max_intg_time.setEnabled (True)
           self.uc.comboBox_output_en.setEnabled (True)
           self.uc.comboBox_qtx2_en.setEnabled (True)
           self.uc.comboBox_baseline_t.setEnabled (True)
           self.uc.comboBox_vth_th1.setEnabled (True)
           self.uc.comboBox_vth_th2.setEnabled (True)
           self.uc.comboBox_vth_e.setEnabled (True)
           self.uc.comboBox_baseline_e.setEnabled (True)
           self.uc.comboBox_fe_delay.setEnabled (True)
           self.uc.comboBox_poststamp_gain_t.setEnabled (True)
           self.uc.comboBox_poststamp_gain_e.setEnabled (True)
           self.uc.comboBox_poststamp_sh_e.setEnabled (True)
           self.uc.comboBox_intg_en.setEnabled (True)
           self.uc.comboBox_intg_signal_en.setEnabled (True)
           self.uc.comboBox_att.setEnabled (True)
           self.uc.comboBox_tdc_current_t.setEnabled (True)
           self.uc.comboBox_tdc_current_e.setEnabled (True)
           self.uc.comboBox_fe_tp_en.setEnabled (True)
           self.uc.comboBox_integ_source_sw.setEnabled (True)
           self.uc.comboBox_t1_hysteresis.setEnabled (True)
           self.uc.comboBox_t2_hysteresis.setEnabled (True)
           self.uc.comboBox_e_hysteresis.setEnabled (True)
           self.uc.comboBox_hysteresis_en_n.setEnabled (True)

           self.uc.checkBox_all_ch.setEnabled (True)
           self.uc.spinBox_ch_number.setEnabled (True)
           self.uc.checkBox_all_ASIC.setEnabled (True)
           self.uc.spinBox_ch_number_2.setEnabled (True)
           self.uc.spinBox_ASIC_n.setEnabled (True)

        elif self.uc.Pass == "Shifter":
           self.uc.Log.setText('Shifter access')
           self.uc.textBrowser.append("Access as Shifter")
           self.uc.textBrowser.append(str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"\n")
           self.uc.d.flags['all_access'] = True
           self.uc.d.flags['shifter_access'] = True

           self.uc.SpinBox_Buffer.setEnabled (True)
           self.uc.SpinBox_Pretrigger.setEnabled (True)
           self.uc.SpinBox_Triggers.setEnabled (True)

           self.uc.comboBox_tx_nlinks.setEnabled (True)
           self.uc.comboBox_tx_ddr.setEnabled (True)
           self.uc.comboBox_tx_mode.setEnabled (True)
           self.uc.checkBox_debug_mode.setEnabled (False)
           self.uc.comboBox_veto_mode.setEnabled (False)
           self.uc.comboBox_tdk_clk_div.setEnabled (False)
           self.uc.checkBox_r_clk_en.setEnabled (False)
           self.uc.checkBox_stop_ramp_en.setEnabled (False)
           self.uc.checkBox_counter_en.setEnabled (False)
           self.uc.comboBox_counter_period.setEnabled (False)
           self.uc.checkBox_tac_refresh_en.setEnabled (False)
           self.uc.comboBox_tac_refresh_period.setEnabled (False)
           self.uc.comboBox_data_clk_div.setEnabled (False)
           self.uc.checkBox_fetp_enable.setEnabled (False)
           self.uc.comboBox_input_polarity.setEnabled (False)
           self.uc.comboBox_attenuator_ls.setEnabled (False)
           self.uc.comboBox_v_ref_diff_bias_ig.setEnabled (False)
           self.uc.comboBox_v_cal_ref_ig.setEnabled (False)
           self.uc.comboBox_fe_postcamp_t.setEnabled (False)
           self.uc.comboBox_fe_postcamp_e.setEnabled (False)
           self.uc.comboBox_v_cal_tp_top.setEnabled (False)
           self.uc.comboBox_v_cal_diff_bias_ig.setEnabled (False)
           self.uc.comboBox_v_att_diff_bias_ig.setEnabled (False)
           self.uc.comboBox_v_integ_ref_ig.setEnabled (False)
           self.uc.comboBox_imirror_bias_top.setEnabled (False)
           self.uc.comboBox_tdc_comp_bias.setEnabled (False)
           self.uc.comboBox_tdc_i_lsb.setEnabled (False)
           self.uc.comboBox_disc_lsb_t1.setEnabled (False)
           self.uc.comboBox_fe_ib2.setEnabled (False)
           self.uc.comboBox_vdiffoldcas.setEnabled (False)
           self.uc.comboBox_disc_vcas.setEnabled (False)
           self.uc.comboBox_disc_lsb_e.setEnabled (False)
           self.uc.comboBox_tdc_i_ref.setEnabled (False)
           self.uc.comboBox_tdc_comp_vcas.setEnabled (False)
           self.uc.comboBox_fe_ib2_x2.setEnabled (False)
           self.uc.comboBox_main_global_dac.setEnabled (False)
           self.uc.comboBox_fe_ib1.setEnabled (False)
           self.uc.comboBox_disc_ib.setEnabled (False)
           self.uc.comboBox_disc_lsb_t2.setEnabled (False)
           self.uc.comboBox_tdc_tac_vcas_p.setEnabled (False)
           self.uc.comboBox_tdc_tac_vcas_n.setEnabled (False)
           self.uc.comboBox_adebug_out_mode.setEnabled (False)
           self.uc.comboBox_tdc_global_dac.setEnabled (False)
           self.uc.comboBox_adebug_buffer.setEnabled (False)
           self.uc.comboBox_disc_sf_bias.setEnabled (False)

           self.uc.comboBox_set_trigger_mode1.setEnabled (True)
           self.uc.comboBox_debug_mode.setEnabled (False)
           self.uc.comboBox_sync_chain_lenght.setEnabled (False)
           self.uc.comboBox_dead_time.setEnabled (False)
           self.uc.comboBox_counter_mode.setEnabled (False)
           self.uc.comboBox_tac_max_age.setEnabled (False)
           self.uc.comboBox_tac_min_age.setEnabled (False)
           self.uc.comboBox_set_trigger_mode2_t.setEnabled (False)
           self.uc.comboBox_set_trigger_mode2_e.setEnabled (False)
           self.uc.comboBox_set_trigger_mode2_q.setEnabled (False)
           self.uc.comboBox_set_trigger_mode2_b.setEnabled (False)
           self.uc.comboBox_branch_en_eq.setEnabled (False)
           self.uc.comboBox_branch_en_t.setEnabled (False)
           self.uc.comboBox_qdc_mode.setEnabled (False)
           self.uc.comboBox_set_trigger_b_latched.setEnabled (False)
           self.uc.comboBox_min_intg_time.setEnabled (False)
           self.uc.comboBox_max_intg_time.setEnabled (False)
           self.uc.comboBox_output_en.setEnabled (False)
           self.uc.comboBox_qtx2_en.setEnabled (False)
           self.uc.comboBox_baseline_t.setEnabled (False)
           self.uc.comboBox_vth_th1.setEnabled (False)
           self.uc.comboBox_vth_th2.setEnabled (False)
           self.uc.comboBox_vth_e.setEnabled (False)
           self.uc.comboBox_baseline_e.setEnabled (False)
           self.uc.comboBox_fe_delay.setEnabled (False)
           self.uc.comboBox_poststamp_gain_t.setEnabled (False)
           self.uc.comboBox_poststamp_gain_e.setEnabled (False)
           self.uc.comboBox_poststamp_sh_e.setEnabled (False)
           self.uc.comboBox_intg_en.setEnabled (False)
           self.uc.comboBox_intg_signal_en.setEnabled (False)
           self.uc.comboBox_att.setEnabled (False)
           self.uc.comboBox_tdc_current_t.setEnabled (False)
           self.uc.comboBox_tdc_current_e.setEnabled (False)
           self.uc.comboBox_fe_tp_en.setEnabled (False)
           self.uc.comboBox_integ_source_sw.setEnabled (False)
           self.uc.comboBox_t1_hysteresis.setEnabled (False)
           self.uc.comboBox_t2_hysteresis.setEnabled (False)
           self.uc.comboBox_e_hysteresis.setEnabled (False)
           self.uc.comboBox_hysteresis_en_n.setEnabled (False)

           self.uc.checkBox_all_ch.setEnabled (True)
           self.uc.spinBox_ch_number.setEnabled (True)
           self.uc.checkBox_all_ASIC.setEnabled (True)
           self.uc.spinBox_ch_number_2.setEnabled (True)
           self.uc.spinBox_ASIC_n.setEnabled (True)

        else:
           self.uc.Log.setText('Wrong password')
           self.uc.d.flags['all_access'] = False
           self.uc.d.flags['shifter_access'] = False

           self.uc.SpinBox_Buffer.setEnabled (False)
           self.uc.SpinBox_Pretrigger.setEnabled (False)
           self.uc.SpinBox_Triggers.setEnabled (False)

           self.uc.comboBox_tx_nlinks.setEnabled (False)
           self.uc.comboBox_tx_ddr.setEnabled (False)
           self.uc.comboBox_tx_mode.setEnabled (False)
           self.uc.checkBox_debug_mode.setEnabled (False)
           self.uc.comboBox_veto_mode.setEnabled (False)
           self.uc.comboBox_tdk_clk_div.setEnabled (False)
           self.uc.checkBox_r_clk_en.setEnabled (False)
           self.uc.checkBox_stop_ramp_en.setEnabled (False)
           self.uc.checkBox_counter_en.setEnabled (False)
           self.uc.comboBox_counter_period.setEnabled (False)
           self.uc.checkBox_tac_refresh_en.setEnabled (False)
           self.uc.comboBox_tac_refresh_period.setEnabled (False)
           self.uc.comboBox_data_clk_div.setEnabled (False)
           self.uc.checkBox_fetp_enable.setEnabled (False)
           self.uc.comboBox_input_polarity.setEnabled (False)
           self.uc.comboBox_attenuator_ls.setEnabled (False)
           self.uc.comboBox_v_ref_diff_bias_ig.setEnabled (False)
           self.uc.comboBox_v_cal_ref_ig.setEnabled (False)
           self.uc.comboBox_fe_postcamp_t.setEnabled (False)
           self.uc.comboBox_fe_postcamp_e.setEnabled (False)
           self.uc.comboBox_v_cal_tp_top.setEnabled (False)
           self.uc.comboBox_v_cal_diff_bias_ig.setEnabled (False)
           self.uc.comboBox_v_att_diff_bias_ig.setEnabled (False)
           self.uc.comboBox_v_integ_ref_ig.setEnabled (False)
           self.uc.comboBox_imirror_bias_top.setEnabled (False)
           self.uc.comboBox_tdc_comp_bias.setEnabled (False)
           self.uc.comboBox_tdc_i_lsb.setEnabled (False)
           self.uc.comboBox_disc_lsb_t1.setEnabled (False)
           self.uc.comboBox_fe_ib2.setEnabled (False)
           self.uc.comboBox_vdiffoldcas.setEnabled (False)
           self.uc.comboBox_disc_vcas.setEnabled (False)
           self.uc.comboBox_disc_lsb_e.setEnabled (False)
           self.uc.comboBox_tdc_i_ref.setEnabled (False)
           self.uc.comboBox_tdc_comp_vcas.setEnabled (False)
           self.uc.comboBox_fe_ib2_x2.setEnabled (False)
           self.uc.comboBox_main_global_dac.setEnabled (False)
           self.uc.comboBox_fe_ib1.setEnabled (False)
           self.uc.comboBox_disc_ib.setEnabled (False)
           self.uc.comboBox_disc_lsb_t2.setEnabled (False)
           self.uc.comboBox_tdc_tac_vcas_p.setEnabled (False)
           self.uc.comboBox_tdc_tac_vcas_n.setEnabled (False)
           self.uc.comboBox_adebug_out_mode.setEnabled (False)
           self.uc.comboBox_tdc_global_dac.setEnabled (False)
           self.uc.comboBox_adebug_buffer.setEnabled (False)
           self.uc.comboBox_disc_sf_bias.setEnabled (False)

           self.uc.comboBox_set_trigger_mode1.setEnabled (False)
           self.uc.comboBox_debug_mode.setEnabled (False)
           self.uc.comboBox_sync_chain_lenght.setEnabled (False)
           self.uc.comboBox_dead_time.setEnabled (False)
           self.uc.comboBox_counter_mode.setEnabled (False)
           self.uc.comboBox_tac_max_age.setEnabled (False)
           self.uc.comboBox_tac_min_age.setEnabled (False)
           self.uc.comboBox_set_trigger_mode2_t.setEnabled (False)
           self.uc.comboBox_set_trigger_mode2_e.setEnabled (False)
           self.uc.comboBox_set_trigger_mode2_q.setEnabled (False)
           self.uc.comboBox_set_trigger_mode2_b.setEnabled (False)
           self.uc.comboBox_branch_en_eq.setEnabled (False)
           self.uc.comboBox_branch_en_t.setEnabled (False)
           self.uc.comboBox_qdc_mode.setEnabled (False)
           self.uc.comboBox_set_trigger_b_latched.setEnabled (False)
           self.uc.comboBox_min_intg_time.setEnabled (False)
           self.uc.comboBox_max_intg_time.setEnabled (False)
           self.uc.comboBox_output_en.setEnabled (False)
           self.uc.comboBox_qtx2_en.setEnabled (False)
           self.uc.comboBox_baseline_t.setEnabled (False)
           self.uc.comboBox_vth_th1.setEnabled (False)
           self.uc.comboBox_vth_th2.setEnabled (False)
           self.uc.comboBox_vth_e.setEnabled (False)
           self.uc.comboBox_baseline_e.setEnabled (False)
           self.uc.comboBox_fe_delay.setEnabled (False)
           self.uc.comboBox_poststamp_gain_t.setEnabled (False)
           self.uc.comboBox_poststamp_gain_e.setEnabled (False)
           self.uc.comboBox_poststamp_sh_e.setEnabled (False)
           self.uc.comboBox_intg_en.setEnabled (False)
           self.uc.comboBox_intg_signal_en.setEnabled (False)
           self.uc.comboBox_att.setEnabled (False)
           self.uc.comboBox_tdc_current_t.setEnabled (False)
           self.uc.comboBox_tdc_current_e.setEnabled (False)
           self.uc.comboBox_fe_tp_en.setEnabled (False)
           self.uc.comboBox_integ_source_sw.setEnabled (False)
           self.uc.comboBox_t1_hysteresis.setEnabled (False)
           self.uc.comboBox_t2_hysteresis.setEnabled (False)
           self.uc.comboBox_e_hysteresis.setEnabled (False)
           self.uc.comboBox_hysteresis_en_n.setEnabled (False)

           self.uc.checkBox_all_ch.setEnabled (False)
           self.uc.spinBox_ch_number.setEnabled (False)
           self.uc.checkBox_all_ASIC.setEnabled (False)
           self.uc.spinBox_ch_number_2.setEnabled (False)
           self.uc.spinBox_ASIC_n.setEnabled (False)

        #print (self.uc.Pass)
        #print(self.uc.d.flags['all_access'])
        #print(self.uc.d.flags['shifter_access'])


class Aux_Buttons():
    """ Configuration of the buttons.
        Method

        Parameters
    """
    def __init__(self,upper_class):
        self.uc = upper_class

    def f_clear(self,option):

        if (option==1):
            self.uc.d.axes['ax1'].cla()
            self.uc.d.axes['ax2'].cla()
            for txt in self.uc.d.fig1.texts:
                txt.set_visible(False)
            self.uc.canvas1.draw()
        elif (option==2):
            self.uc.d.axes['ax3'].cla()
            for txt in self.uc.d.fig2.texts:
                txt.set_visible(False)
            self.uc.canvas2.draw()
        elif (option==3):
            self.uc.d.axes['ax4'].cla()
            for txt in self.uc.d.fig3.texts:
                txt.set_visible(False)
            self.uc.canvas3.draw()
        elif (option==4):
            self.uc.d.axes['ax5'].cla()
            for txt in self.uc.d.fig4.texts:
                txt.set_visible(False)
            self.uc.canvas4.draw()

        #self.SpinBox_Buffer.clear()
        #self.SpinBox_Pretrigger.clear()
        #self.SpinBox_Triggers.clear()

    def f_save0(self):

        with open(str(self.uc.lineEdit_conf_file.text())+'.json',"w") as write_file:
       	    json.dump(self.uc.d.Config_data, write_file)

    def f_save1(self):

    	#print(self.uc.d.Config_data)

    	self.uc.d.Config_data['time_r'] = self.int_v(self.uc.SpinBox_Buffer.value())
    	self.uc.d.Config_data['events'] = self.int_v(self.uc.SpinBox_Pretrigger.value())
    	self.uc.d.Config_data['f_size'] = self.int_v(self.uc.SpinBox_Triggers.value())


    def f_save_globConfig(self):

        with open('/Users/vins/Documents/IFIC/PETALO/PYTHON/Configurations/Config_global_ASIC.json',"w") as write_file:
       	    json.dump(str(self.uc.asi_c.bit_glob_write_conf), write_file)

    def f_save_chConfig(self):

        with open('/Users/vins/Documents/IFIC/PETALO/PYTHON/Configurations/Config_ch_ASIC.json',"w") as write_file:
       	    json.dump(str(self.uc.asi_c.bit_ch), write_file)


    def f_write0(self):   #read file .json

    	with open(str(self.uc.d.site2), "r") as read_file:
    		 data = json.load(read_file)

    	#print (type(data))

    	self.uc.d.Config_data['time_r'] = data['time_r']
    	self.uc.d.Config_data['events'] = data['events']
    	self.uc.d.Config_data['f_size'] = data['f_size']


    def f_write1(self):    #write data in the log

        self.uc.textBrowser.append("Registers loaded:")
        self.uc.textBrowser.append("Time of adquisition: "+str(self.uc.d.Config_data['time_r'])+" min.")
        self.uc.textBrowser.append("Number of events: "+str(self.uc.d.Config_data['events']))
        self.uc.textBrowser.append("Size of the files: "+str(self.uc.d.Config_data['f_size'])+" Mb")
        self.uc.textBrowser.append(str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"\n")


    def f_load(self):

        #print(self.uc.d.Registers)

        self.uc.SpinBox_Buffer.setValue(self.int_v(self.uc.d.Config_data['time_r']))
        self.uc.SpinBox_Pretrigger.setValue(self.int_v(self.uc.d.Config_data['events']))
        self.uc.SpinBox_Triggers.setValue(self.int_v(self.uc.d.Config_data['f_size']))

# Controlled casting to avoid data intro errors
    def float_v(self,number):
        try:
            return float(number)
        except ValueError:
            return 0.0

    def int_v(self,number):
        try:
            return int(number)
        except ValueError:
            return 0

    def store_data(self):

        self.uc.d.files['dir'] = self.uc.lineEdit_plot_file_path.text()
        self.uc.d.site2 = self.uc.lineEdit_config_file_path.text()

        self.uc.d.Config_data['time_r'] = self.int_v(self.uc.SpinBox_Buffer.value())
        self.uc.d.Config_data['events'] = self.int_v(self.uc.SpinBox_Pretrigger.value())
        self.uc.d.Config_data['f_size'] = self.int_v(self.uc.SpinBox_Triggers.value())

        self.uc.d.files['conf_name'] = self.uc.lineEdit_conf_file.text()

class Browsers():
    """ Browsers configuration for the program.
            Method

            Parameters

    """
    def __init__(self,upper_class):
        self.uc = upper_class

    def path_browser_config(self):
        file_aux = QtWidgets.QFileDialog.getOpenFileName(self.uc,
                                        'Open file',
                                        self.uc.d.site2,
                                        "Register data (*.json)")

        fname_aux = ([str(x) for x in file_aux])
        self.uc.d.site2 = fname_aux[0]
        #Trick for Qstring converting to standard string
        self.uc.lineEdit_config_file_path.setText(self.uc.d.site2)

    def path_browser_plot(self):
        file_aux = QtWidgets.QFileDialog.getOpenFileName(self.uc,
                                        'Open file',
                                        self.uc.d.site,
                                        "Plot file (*.h5)")

        fname_aux = ([str(x) for x in file_aux])
        self.uc.d.site = fname_aux[0]
        #Trick for Qstring converting to standard string
        self.uc.lineEdit_plot_file_path.setText(self.uc.d.site)


class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)

        #Data class
        self.d = LAB_data()
        self.c = CORE(self)
        self.browser_tools = Browsers(self)
        self.b_buttons = Aux_Buttons(self)
        self.asi_c = ASIC_param.AsicConfig(self)
        self.asi_p = ASIC_param.ASIC_param(self)
        # Passes all the needed information to the constructor of the aux classes

        self.plot_Button.clicked.connect(lambda:self.frame_plot_button.setStyleSheet(" .QFrame { background-color : grey } "))
        self.Clear_button_1.clicked.connect(lambda:self.frame_plot_button.setStyleSheet(" .QFrame { background-color : base } "))


        self.checkBox_all_ASIC.clicked.connect(lambda:self.frame_all_ASICs.setStyleSheet(" .QFrame { background-color : grey } "))

        #self.comboBox_tx_nlinks.setStyleSheet("background-color: white;")
        self.comboBox_tx_nlinks.currentIndexChanged.connect(lambda:self.comboBox_tx_nlinks.setStyleSheet("background-color: grey;"))

        #Button Calls

        self.toolButton_plot.clicked.connect(self.browser_tools.path_browser_plot)
        self.toolButton_config.clicked.connect(self.browser_tools.path_browser_config)

        self.pushButton_config.clicked.connect(self.b_buttons.f_save0)
        self.pushButton_config.clicked.connect(self.b_buttons.f_save1)
        self.pushButton_load.clicked.connect(self.b_buttons.f_write0)
        self.pushButton_load.clicked.connect(self.b_buttons.f_write1)
        self.pushButton_load.clicked.connect(self.b_buttons.f_load)
        self.pushButton_config.clicked.connect(self.c.Config_reg)

        self.START.clicked.connect(self.c.Start_run)
       	self.STOP.clicked.connect(self.c.Stop_run)

       	self.pushButton_reg_glob.clicked.connect(self.asi_c.Config_update_glob)
        self.pushButton_reg_glob.clicked.connect(lambda:self.frame_all_ASICs.setStyleSheet(" .QFrame { background-color : base } "))
        self.pushButton_reg_glob.clicked.connect(lambda:self.comboBox_tx_nlinks.setStyleSheet("background-color: base;"));

        self.pushButton_reg_ch.clicked.connect(self.asi_c.Config_update_ch)

        self.Clear_button_1.clicked.connect(lambda:self.b_buttons.f_clear(1))
        self.plot_Button.clicked.connect(self.c.f_PLOT)


		#Defaults

        #Main page

        self.pass_lineEdit.editingFinished.connect(self.c.validate_pass)
        self.pass_lineEdit.setText(self.d.Pass)

        self.lineEdit_config_file_path.setText(self.d.site2)
        self.lineEdit_conf_file.setText(str(self.d.files['conf_name']))
        self.lineEdit_config_file_path.editingFinished.connect(self.b_buttons.store_data)
        self.lineEdit_conf_file.editingFinished.connect(self.b_buttons.store_data)

        self.SpinBox_Buffer.setEnabled (False)
        self.SpinBox_Pretrigger.setEnabled (False)
        self.SpinBox_Triggers.setEnabled (False)

        self.SpinBox_Buffer.setValue(self.d.Config_data['time_r'])
        self.SpinBox_Pretrigger.setValue(self.d.Config_data['events'])
        self.SpinBox_Triggers.setValue(self.d.Config_data['f_size'])

        self.SpinBox_Buffer.editingFinished.connect(self.b_buttons.store_data)
        self.SpinBox_Pretrigger.editingFinished.connect(self.b_buttons.store_data)
        self.SpinBox_Triggers.editingFinished.connect(self.b_buttons.store_data)

        #Plot page

        self.lineEdit_plot_file_path.setText(self.d.site)
        self.lineEdit_plot_file_path.editingFinished.connect(self.b_buttons.store_data)


        #Global config paramenters

        self.comboBox_tx_nlinks.setCurrentIndex(2)
        self.comboBox_tx_ddr.setCurrentIndex(1)
        self.comboBox_tx_mode.setCurrentIndex(2)
        self.checkBox_debug_mode.setChecked(False)
        self.comboBox_veto_mode.setCurrentIndex(0)
        self.comboBox_tdk_clk_div.setCurrentIndex(1)
        self.checkBox_r_clk_en.setChecked(True)
        self.checkBox_stop_ramp_en.setChecked(True)
        self.checkBox_counter_en.setChecked(True)
        self.comboBox_counter_period.setCurrentIndex(0)
        self.checkBox_tac_refresh_en.setChecked(True)
        self.comboBox_tac_refresh_period.setCurrentIndex(0)
        self.comboBox_data_clk_div.setCurrentIndex(0)
        self.checkBox_fetp_enable.setChecked(False)
        self.comboBox_input_polarity.setCurrentIndex(0)
        self.comboBox_attenuator_ls.setCurrentIndex(0)
        self.comboBox_v_ref_diff_bias_ig.setCurrentIndex(0)
        self.comboBox_v_cal_ref_ig.setCurrentIndex(0)
        self.comboBox_fe_postcamp_t.setCurrentIndex(0)
        self.comboBox_fe_postcamp_e.setCurrentIndex(0)
        self.comboBox_v_cal_tp_top.setCurrentIndex(0)
        self.comboBox_v_cal_diff_bias_ig.setCurrentIndex(0)
        self.comboBox_v_att_diff_bias_ig.setCurrentIndex(0)
        self.comboBox_v_integ_ref_ig.setCurrentIndex(0)
        self.comboBox_imirror_bias_top.setCurrentIndex(0)
        self.comboBox_tdc_comp_bias.setCurrentIndex(0)
        self.comboBox_tdc_i_lsb.setCurrentIndex(0)
        self.comboBox_disc_lsb_t1.setCurrentIndex(0)
        self.comboBox_fe_ib2.setCurrentIndex(0)
        self.comboBox_vdiffoldcas.setCurrentIndex(0)
        self.comboBox_disc_vcas.setCurrentIndex(0)
        self.comboBox_disc_lsb_e.setCurrentIndex(0)
        self.comboBox_tdc_i_ref.setCurrentIndex(0)
        self.comboBox_tdc_comp_vcas.setCurrentIndex(0)
        self.comboBox_fe_ib2_x2.setCurrentIndex(0)
        self.comboBox_main_global_dac.setCurrentIndex(0)
        self.comboBox_fe_ib1.setCurrentIndex(0)
        self.comboBox_disc_ib.setCurrentIndex(0)
        self.comboBox_disc_lsb_t2.setCurrentIndex(0)
        self.comboBox_tdc_tac_vcas_p.setCurrentIndex(0)
        self.comboBox_tdc_tac_vcas_n.setCurrentIndex(0)
        self.comboBox_adebug_out_mode.setCurrentIndex(0)
        self.comboBox_tdc_global_dac.setCurrentIndex(0)
        self.comboBox_adebug_buffer.setCurrentIndex(0)
        self.comboBox_disc_sf_bias.setCurrentIndex(0)

        self.comboBox_tx_nlinks.setEnabled (False)
        self.comboBox_tx_ddr.setEnabled (False)
        self.comboBox_tx_mode.setEnabled (False)
        self.checkBox_debug_mode.setEnabled (False)
        self.comboBox_veto_mode.setEnabled (False)
        self.comboBox_tdk_clk_div.setEnabled (False)
        self.checkBox_r_clk_en.setEnabled (False)
        self.checkBox_stop_ramp_en.setEnabled (False)
        self.checkBox_counter_en.setEnabled (False)
        self.comboBox_counter_period.setEnabled (False)
        self.checkBox_tac_refresh_en.setEnabled (False)
        self.comboBox_tac_refresh_period.setEnabled (False)
        self.comboBox_data_clk_div.setEnabled (False)
        self.checkBox_fetp_enable.setEnabled (False)
        self.comboBox_input_polarity.setEnabled (False)
        self.comboBox_attenuator_ls.setEnabled (False)
        self.comboBox_v_ref_diff_bias_ig.setEnabled (False)
        self.comboBox_v_cal_ref_ig.setEnabled (False)
        self.comboBox_fe_postcamp_t.setEnabled (False)
        self.comboBox_fe_postcamp_e.setEnabled (False)
        self.comboBox_v_cal_tp_top.setEnabled (False)
        self.comboBox_v_cal_diff_bias_ig.setEnabled (False)
        self.comboBox_v_att_diff_bias_ig.setEnabled (False)
        self.comboBox_v_integ_ref_ig.setEnabled (False)
        self.comboBox_imirror_bias_top.setEnabled (False)
        self.comboBox_tdc_comp_bias.setEnabled (False)
        self.comboBox_tdc_i_lsb.setEnabled (False)
        self.comboBox_disc_lsb_t1.setEnabled (False)
        self.comboBox_fe_ib2.setEnabled (False)
        self.comboBox_vdiffoldcas.setEnabled (False)
        self.comboBox_disc_vcas.setEnabled (False)
        self.comboBox_disc_lsb_e.setEnabled (False)
        self.comboBox_tdc_i_ref.setEnabled (False)
        self.comboBox_tdc_comp_vcas.setEnabled (False)
        self.comboBox_fe_ib2_x2.setEnabled (False)
        self.comboBox_main_global_dac.setEnabled (False)
        self.comboBox_fe_ib1.setEnabled (False)
        self.comboBox_disc_ib.setEnabled (False)
        self.comboBox_disc_lsb_t2.setEnabled (False)
        self.comboBox_tdc_tac_vcas_p.setEnabled (False)
        self.comboBox_tdc_tac_vcas_n.setEnabled (False)
        self.comboBox_adebug_out_mode.setEnabled (False)
        self.comboBox_tdc_global_dac.setEnabled (False)
        self.comboBox_adebug_buffer.setEnabled (False)
        self.comboBox_disc_sf_bias.setEnabled (False)

        self.checkBox_all_ASIC.setEnabled (False)
        self.spinBox_ch_number_2.setEnabled (False)
        self.spinBox_ASIC_n.setEnabled (False)

        self.checkBox_all_ASIC.clicked.connect(self.asi_p.n_ASIC_conf)

        #Channel ASIC parameters

        self.comboBox_set_trigger_mode1.setCurrentIndex(0)
        self.comboBox_debug_mode.setCurrentIndex(0)
        self.comboBox_sync_chain_lenght.setCurrentIndex(0)
        self.comboBox_dead_time.setCurrentIndex(0)
        self.comboBox_counter_mode.setCurrentIndex(0)
        self.comboBox_tac_max_age.setCurrentIndex(0)
        self.comboBox_tac_min_age.setCurrentIndex(0)
        self.comboBox_set_trigger_mode2_t.setCurrentIndex(1)
        self.comboBox_set_trigger_mode2_e.setCurrentIndex(2)
        self.comboBox_set_trigger_mode2_q.setCurrentIndex(1)
        self.comboBox_set_trigger_mode2_b.setCurrentIndex(5)
        self.comboBox_branch_en_eq.setCurrentIndex(0)
        self.comboBox_branch_en_t.setCurrentIndex(0)
        self.comboBox_qdc_mode.setCurrentIndex(0)
        self.comboBox_set_trigger_b_latched.setCurrentIndex(0)
        self.comboBox_min_intg_time.setCurrentIndex(0)
        self.comboBox_max_intg_time.setCurrentIndex(0)
        self.comboBox_output_en.setCurrentIndex(0)
        self.comboBox_qtx2_en.setCurrentIndex(0)
        self.comboBox_baseline_t.setCurrentIndex(0)
        self.comboBox_vth_th1.setCurrentIndex(0)
        self.comboBox_vth_th2.setCurrentIndex(0)
        self.comboBox_vth_e.setCurrentIndex(0)
        self.comboBox_baseline_e.setCurrentIndex(0)
        self.comboBox_fe_delay.setCurrentIndex(1)
        self.comboBox_poststamp_gain_t.setCurrentIndex(0)
        self.comboBox_poststamp_gain_e.setCurrentIndex(0)
        self.comboBox_poststamp_sh_e.setCurrentIndex(0)
        self.comboBox_intg_en.setCurrentIndex(0)
        self.comboBox_intg_signal_en.setCurrentIndex(0)
        self.comboBox_att.setCurrentIndex(1)
        self.comboBox_tdc_current_t.setCurrentIndex(0)
        self.comboBox_tdc_current_e.setCurrentIndex(0)
        self.comboBox_fe_tp_en.setCurrentIndex(0)
        self.comboBox_integ_source_sw.setCurrentIndex(0)
        self.comboBox_t1_hysteresis.setCurrentIndex(0)
        self.comboBox_t2_hysteresis.setCurrentIndex(0)
        self.comboBox_e_hysteresis.setCurrentIndex(0)
        self.comboBox_hysteresis_en_n.setCurrentIndex(0)

        self.comboBox_set_trigger_mode1.setEnabled (False)
        self.comboBox_debug_mode.setEnabled (False)
        self.comboBox_sync_chain_lenght.setEnabled (False)
        self.comboBox_dead_time.setEnabled (False)
        self.comboBox_counter_mode.setEnabled (False)
        self.comboBox_tac_max_age.setEnabled (False)
        self.comboBox_tac_min_age.setEnabled (False)
        self.comboBox_set_trigger_mode2_t.setEnabled (False)
        self.comboBox_set_trigger_mode2_e.setEnabled (False)
        self.comboBox_set_trigger_mode2_q.setEnabled (False)
        self.comboBox_set_trigger_mode2_b.setEnabled (False)
        self.comboBox_branch_en_eq.setEnabled (False)
        self.comboBox_branch_en_t.setEnabled (False)
        self.comboBox_qdc_mode.setEnabled (False)
        self.comboBox_set_trigger_b_latched.setEnabled (False)
        self.comboBox_min_intg_time.setEnabled (False)
        self.comboBox_max_intg_time.setEnabled (False)
        self.comboBox_output_en.setEnabled (False)
        self.comboBox_qtx2_en.setEnabled (False)
        self.comboBox_baseline_t.setEnabled (False)
        self.comboBox_vth_th1.setEnabled (False)
        self.comboBox_vth_th2.setEnabled (False)
        self.comboBox_vth_e.setEnabled (False)
        self.comboBox_baseline_e.setEnabled (False)
        self.comboBox_fe_delay.setEnabled (False)
        self.comboBox_poststamp_gain_t.setEnabled (False)
        self.comboBox_poststamp_gain_e.setEnabled (False)
        self.comboBox_poststamp_sh_e.setEnabled (False)
        self.comboBox_intg_en.setEnabled (False)
        self.comboBox_intg_signal_en.setEnabled (False)
        self.comboBox_att.setEnabled (False)
        self.comboBox_tdc_current_t.setEnabled (False)
        self.comboBox_tdc_current_e.setEnabled (False)
        self.comboBox_fe_tp_en.setEnabled (False)
        self.comboBox_integ_source_sw.setEnabled (False)
        self.comboBox_t1_hysteresis.setEnabled (False)
        self.comboBox_t2_hysteresis.setEnabled (False)
        self.comboBox_e_hysteresis.setEnabled (False)
        self.comboBox_hysteresis_en_n.setEnabled (False)

        self.checkBox_all_ch.setEnabled (False)
        self.spinBox_ch_number.setEnabled (False)

        self.checkBox_all_ch.clicked.connect(self.asi_p.n_ch_conf)


    def addmpl_1(self, fig):
        # Matplotlib constructor
        self.canvas1 = FigureCanvas(fig)
        self.mpl_lay.addWidget(self.canvas1)
        self.canvas1.draw()
        self.toolbar = NavigationToolbar(self.canvas1, self.frame_plot,coordinates=True)
        self.mpl_lay.addWidget(self.toolbar)
        self.d.axes['ax1'] = fig.add_subplot(111)
        self.d.axes['ax2'] = self.d.axes['ax1'].twinx()
        self.d.axes['ax1'].set_xlabel("Samples")
        self.d.axes['ax1'].set_ylabel("ADC counts")
        self.d.axes['ax1'].legend(" ")


if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.addmpl_1(window.d.fig1)
    window.show()
    sys.exit(app.exec_())
