from bitarray import bitarray
from Libraries import bitarray_utils
#from bitarray_utils import intToBin, binToInt, grayToBin, grayToInt
from Libraries import crc8
import binascii

def nrange(start, end):
    r = [x for x in range(start, end) ]
    r.reverse()
    return r

def crc_8(_CRC):

    _CRC = _CRC.tobytes()
    print(_CRC)

    #print(b'1011000000001110000001101100100001101111111001111111011110100000010000010001111101110111001001001111101011000110110111010100010010001011011111101101001111000001101011111010011000100000')
    #_CRC = 'B00E06C86FE7F7A0411F7724FAC6DD448B7ED3C1AFA620'
    #_CRC = 'B00E06C86FE7F7A0411F7724FAC6DD448B7ED3C1AFA62080'
    #_CRC = bytes.fromhex(_CRC)
    #print(_CRC)

    crc_8 = crc8.crc8()
    #crc_8.update(b'\xB0\x0E\x06\xC8\x6F\xE7\xF7\xA0\x41\x1F\x77\x24\xFA\xC6\xDD\x44\x8B\x7E\xD3\xC1\xAF\xA6\x20\x80')
    crc_8.update(_CRC)
    print (crc_8.hexdigest())
    crc_8.digest()
    a = bin(int(binascii.hexlify(crc_8.digest()), 16))
    a = a.replace("0b","")
    while len(a) < 8:
        a =  '0' + a
    print(a)

    return a


class ASIC_param():
    """ Read data from the program to config the ASIC parameters.
            Method

            Parameters

    """

    def __init__(self,upper_class):
        self.uc = upper_class

    def selec_glob(self):

        tx_nlinks_index = self.uc.comboBox_tx_nlinks.currentIndex()
        tx_ddr_index = self.uc.comboBox_tx_ddr.currentIndex()
        tx_mode_index = self.uc.comboBox_tx_mode.currentIndex()
        veto_mode_index = self.uc.comboBox_veto_mode.currentIndex()
        tdk_clk_div_index = self.uc.comboBox_tdk_clk_div.currentIndex()
        counter_period_index = self.uc.comboBox_counter_period.currentIndex()
        tac_refresh_period_index = self.uc.comboBox_tac_refresh_period.currentIndex()
        data_clk_div_index = self.uc.comboBox_data_clk_div.currentIndex()
        input_polarity_index = self.uc.comboBox_input_polarity.currentIndex()
        attenuator_ls_index = self.uc.comboBox_attenuator_ls.currentIndex()
        v_ref_diff_bias_ig_index = self.uc.comboBox_v_ref_diff_bias_ig.currentIndex()
        v_cal_ref_ig_index = self.uc.comboBox_v_cal_ref_ig.currentIndex()
        fe_postcamp_t_index = self.uc.comboBox_fe_postcamp_t.currentIndex()
        fe_postcamp_e_index = self.uc.comboBox_fe_postcamp_e.currentIndex()
        v_cal_tp_top_index = self.uc.comboBox_v_cal_tp_top.currentIndex()
        v_cal_diff_bias_ig_index = self.uc.comboBox_v_cal_diff_bias_ig.currentIndex()
        v_att_diff_bias_ig_index = self.uc.comboBox_v_att_diff_bias_ig.currentIndex()
        v_integ_ref_ig_index = self.uc.comboBox_v_integ_ref_ig.currentIndex()
        imirror_bias_top_index = self.uc.comboBox_imirror_bias_top.currentIndex()
        tdc_comp_bias_index = self.uc.comboBox_tdc_comp_bias.currentIndex()
        tdc_i_lsb_index = self.uc.comboBox_tdc_i_lsb.currentIndex()
        disc_lsb_t1_index = self.uc.comboBox_disc_lsb_t1.currentIndex()
        fe_ib2_index = self.uc.comboBox_fe_ib2.currentIndex()
        vdiffoldcas_index = self.uc.comboBox_vdiffoldcas.currentIndex()
        disc_vcas_index = self.uc.comboBox_disc_vcas.currentIndex()
        disc_lsb_e_index = self.uc.comboBox_disc_lsb_e.currentIndex()
        tdc_i_ref_index = self.uc.comboBox_tdc_i_ref.currentIndex()
        tdc_comp_vcas_index = self.uc.comboBox_tdc_comp_vcas.currentIndex()
        fe_ib2_x2_index = self.uc.comboBox_fe_ib2_x2.currentIndex()
        main_global_dac_index = self.uc.comboBox_main_global_dac.currentIndex()
        fe_ib1_index = self.uc.comboBox_fe_ib1.currentIndex()
        disc_ib_index = self.uc.comboBox_disc_ib.currentIndex()
        disc_lsb_t2_index = self.uc.comboBox_disc_lsb_t2.currentIndex()
        tdc_tac_vcas_p_index = self.uc.comboBox_tdc_tac_vcas_p.currentIndex()
        tdc_tac_vcas_n_index = self.uc.comboBox_tdc_tac_vcas_n.currentIndex()
        adebug_out_mode_index = self.uc.comboBox_adebug_out_mode.currentIndex()
        tdc_global_dac_index = self.uc.comboBox_tdc_global_dac.currentIndex()
        adebug_buffer_index = self.uc.comboBox_adebug_buffer.currentIndex()
        disc_sf_bias_index = self.uc.comboBox_disc_sf_bias.currentIndex()


        if tx_nlinks_index == 0:
            a = bitarray('00')
        elif tx_nlinks_index == 1:
            a = bitarray('01')
        else:
            a = bitarray('10')

        if tx_ddr_index == 0:
        	b = bitarray('0')
        else:
            b = bitarray('1')

        if tx_mode_index == 0:
         	c = bitarray('00')
        elif tx_mode_index == 1:
            c = bitarray('01')
        else:
         	c = bitarray('10')

        if (self.uc.checkBox_debug_mode.isChecked() == True):
        	d = bitarray('1')
        elif (self.uc.checkBox_debug_mode.isChecked() == False):
         	d = bitarray('0')

        if veto_mode_index == 0:
        	e = bitarray('000000')
        elif veto_mode_index == 1:
        	e = bitarray('000001')
        elif veto_mode_index == 2:
        	e = bitarray('000010')
        else:
            e = bitarray('000011')

        if tdk_clk_div_index == 0:
        	f = bitarray('0')
        else:
        	f = bitarray('1')

        if (self.uc.checkBox_r_clk_en.isChecked() == True):
            g = bitarray('110')
        elif (self.uc.checkBox_r_clk_en.isChecked() == False):
            g = bitarray('000')

        if (self.uc.checkBox_stop_ramp_en.isChecked() == True):
        	h = bitarray('00')
        elif (self.uc.checkBox_stop_ramp_en.isChecked() == False):
        	h = bitarray('01')

        if (self.uc.checkBox_counter_en.isChecked() == True):
        	i = bitarray('0')
        elif (self.uc.checkBox_counter_en.isChecked() == False):
            i = bitarray('1')

        if counter_period_index == 0:
        	j = bitarray('110')
        else:
        	j = bitarray('111')

        if (self.uc.checkBox_tac_refresh_en.isChecked() == True):
        	k = bitarray('1')
        elif (self.uc.checkBox_tac_refresh_en.isChecked() == False):
         	k = bitarray('0')

        if tac_refresh_period_index == 0:
        	l = bitarray('1001')

        if data_clk_div_index == 0:
            m = bitarray('00')

        if (self.uc.checkBox_fetp_enable.isChecked() == True):
        	n = bitarray('1')
        elif (self.uc.checkBox_fetp_enable.isChecked() == False):
            n = bitarray('0')

        if input_polarity_index == 0:
            o = bitarray('1')

        if attenuator_ls_index == 0:
            p = bitarray('101111')

        if v_ref_diff_bias_ig_index  == 0:
            q = bitarray('111001')

        if v_cal_ref_ig_index  == 0:
            r = bitarray('11111')

        if fe_postcamp_t_index  == 0:
            s = bitarray('10111')

        if fe_postcamp_e_index  == 0:
            t = bitarray('10100')

        if v_cal_tp_top_index  == 0:
            u = bitarray('00001')


        if v_cal_diff_bias_ig_index  == 0:
            v = bitarray('00000')


        if v_att_diff_bias_ig_index  == 0:
            w = bitarray('100011')

        if v_integ_ref_ig_index  == 0:
            x = bitarray('111011')

        if imirror_bias_top_index  == 0:
            y = bitarray('10111')

        if tdc_comp_bias_index  == 0:
        	aa = bitarray('00100')

        if tdc_i_lsb_index  == 0:
            bb = bitarray('10011')

        if disc_lsb_t1_index  == 0:
            cc = bitarray('111010')

        if fe_ib2_index  == 0:
            dd = bitarray('110000')

        if vdiffoldcas_index == 0:
        	ee = bitarray('110110')

        if disc_vcas_index == 0:
        	ff = bitarray('1110')

        if disc_lsb_e_index  == 0:
            gg = bitarray('101000')

        if tdc_i_ref_index  == 0:
            hh = bitarray('10010')

        if tdc_comp_vcas_index  == 0:
            ii = bitarray('0010')

        if fe_ib2_x2_index  == 0:
            jj = bitarray('1')

        if main_global_dac_index  == 0:
            kk = bitarray('10111')

        if fe_ib1_index  == 0:
            ll = bitarray('111011')

        if disc_ib_index  == 0:
            mm = bitarray('010011')

        if disc_lsb_t2_index  == 0:
        	nn = bitarray('110000')

        if tdc_tac_vcas_p_index  == 0:
        	oo = bitarray('01101')

        if tdc_tac_vcas_n_index  == 0:
        	pp = bitarray('0111')

        if adebug_out_mode_index  == 0:
            qq = bitarray('11')

        if tdc_global_dac_index  == 0:
            rr = bitarray('010011')

        if adebug_buffer_index  == 0:
            ss = bitarray('1')

        if disc_sf_bias_index  == 0:
            tt = bitarray('100000')

        return a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,nn,oo,pp,qq,rr,ss,tt

    def selec_ch(self):

        trigger_mode_1_index = self.uc.comboBox_set_trigger_mode1.currentIndex()
        debug_mode_index = self.uc.comboBox_debug_mode.currentIndex()
        sync_chain_length_index =self.uc.comboBox_sync_chain_lenght.currentIndex()
        dead_time_index =self.uc.comboBox_dead_time.currentIndex()
        counter_mode_index =self.uc.comboBox_counter_mode.currentIndex()
        tac_max_age_index =self.uc.comboBox_tac_max_age.currentIndex()
        tac_min_age_index =self.uc.comboBox_tac_min_age.currentIndex()
        trigger_mode_2_t_index =self.uc.comboBox_set_trigger_mode2_t.currentIndex()
        trigger_mode_2_e_index =self.uc.comboBox_set_trigger_mode2_e.currentIndex()
        trigger_mode_2_q_index =self.uc.comboBox_set_trigger_mode2_q.currentIndex()
        trigger_mode_2_b_index =self.uc.comboBox_set_trigger_mode2_b.currentIndex()
        branch_en_eq_index =self.uc.comboBox_branch_en_eq.currentIndex()
        branch_en_t_index =self.uc.comboBox_branch_en_t.currentIndex()
        qdc_mode_index =self.uc.comboBox_qdc_mode.currentIndex()
        trigger_b_latched_index =self.uc.comboBox_set_trigger_b_latched.currentIndex()
        min_intg_time_index =self.uc.comboBox_min_intg_time.currentIndex()
        max_intg_time_index =self.uc.comboBox_max_intg_time.currentIndex()
        output_en_index =self.uc.comboBox_output_en.currentIndex()
        qtx2_en_index =self.uc.comboBox_qtx2_en.currentIndex()
        baseline_t_index =self.uc.comboBox_baseline_t.currentIndex()
        vh_t1_index =self.uc.comboBox_vth_th1.currentIndex()
        vth_t2_index =self.uc.comboBox_vth_th2.currentIndex()
        vth_e_index =self.uc.comboBox_vth_e.currentIndex()
        baseline_e_index =self.uc.comboBox_baseline_e.currentIndex()
        fe_delay_index =self.uc.comboBox_fe_delay.currentIndex()
        postamp_gain_t_index =self.uc.comboBox_poststamp_gain_t.currentIndex()
        postamp_gain_e_index =self.uc.comboBox_poststamp_gain_e.currentIndex()
        postamp_sh_e_index =self.uc.comboBox_poststamp_sh_e.currentIndex()
        intg_en_index =self.uc.comboBox_intg_en.currentIndex()
        intg_signal_en_index =self.uc.comboBox_intg_signal_en.currentIndex()
        att_index =self.uc.comboBox_att.currentIndex()
        tdc_current_t_index =self.uc.comboBox_tdc_current_t.currentIndex()
        tdc_current_e_index =self.uc.comboBox_tdc_current_e.currentIndex()
        fe_tp_en_index =self.uc.comboBox_fe_tp_en.currentIndex()
        integ_source_sw_index =self.uc.comboBox_integ_source_sw.currentIndex()
        t1_hysteresis_index =self.uc.comboBox_t1_hysteresis.currentIndex()
        t2_hysteresis_index =self.uc.comboBox_t2_hysteresis.currentIndex()
        e_hysteresis_index =self.uc.comboBox_e_hysteresis.currentIndex()
        hysteresis_en_n_index =self.uc.comboBox_hysteresis_en_n.currentIndex()

        if trigger_mode_1_index == 0:
            xx = bitarray('00')
        elif trigger_mode_1_index == 1:
            xx = bitarray('01')
        elif trigger_mode_1_index == 2:
            xx = bitarray('10')
        else:
            xx = bitarray('11')


        if debug_mode_index == 0:
            a1 = bitarray('00')
        elif debug_mode_index == 1:
            a1 = bitarray('00')
        elif debug_mode_index == 2:
            a1 = bitarray('01')
        else:
            a1 = bitarray('10')

        if 	sync_chain_length_index == 0:
        	b1 = bitarray('00')

        if dead_time_index == 0:
        	c1 = bitarray('000000')

        if counter_mode_index == 0:
        	d1 = bitarray('0000')
        elif counter_mode_index == 1:
        	d1 = bitarray('0001')
        elif counter_mode_index == 2:
        	d1 = bitarray('0010')
        elif counter_mode_index == 3:
        	d1 = bitarray('0011')
        elif counter_mode_index == 4:
        	d1 = bitarray('1000')
        elif counter_mode_index == 5:
        	d1 = bitarray('1100')
        else:
        	d1 = bitarray('1111')

        if tac_max_age_index== 0:
        	e1 = bitarray('11110')

        if tac_min_age_index == 0:
        	f1 = bitarray('01010')

        if trigger_mode_2_t_index == 0:
            g1 = bitarray('00')
        elif trigger_mode_2_t_index == 1:
            g1 = bitarray('01')
        else:
            g1 = bitarray('10')

        if trigger_mode_2_e_index == 0:
            h1 = bitarray('000')
        elif trigger_mode_2_e_index == 1:
            h1 = bitarray('001')
        elif trigger_mode_2_e_index == 2:
            h1 = bitarray('010')
        elif trigger_mode_2_e_index == 3:
            h1 = bitarray('011')
        elif trigger_mode_2_e_index == 4:
            h1 = bitarray('100')
        elif trigger_mode_2_e_index == 5:
            h1 = bitarray('101')
        elif trigger_mode_2_e_index == 6:
            h1 = bitarray('110')
        else:
            h1 = bitarray('111')

        if trigger_mode_2_q_index == 0:
            i1 = bitarray('00')
        elif trigger_mode_2_q_index == 1:
            i1 = bitarray('01')
        else:
            i1 = bitarray('10')

        if trigger_mode_2_b_index == 0:
            j1 = bitarray('000')
        elif trigger_mode_2_b_index == 1:
            j1 = bitarray('001')
        elif trigger_mode_2_b_index == 2:
            j1 = bitarray('010')
        elif trigger_mode_2_b_index == 3:
            j1 = bitarray('011')
        elif trigger_mode_2_b_index == 4:
            j1 = bitarray('100')
        else:
            j1 = bitarray('101')

        if branch_en_eq_index == 0:
            k1 = bitarray('1')

        if branch_en_t_index == 0:
            l1 = bitarray('1')

        if qdc_mode_index == 0:
            m1 = bitarray('1')
        else:
            m1 = bitarray('0')

        if trigger_b_latched_index== 0:
            n1 = bitarray('0')

        if min_intg_time_index == 0:
            o1 = bitarray('100010')

        if max_intg_time_index == 0:
            p1 = bitarray('100010')

        if output_en_index == 0:
            q1 = bitarray('00')

        if qtx2_en_index == 0:
            r1 = bitarray('0')

        if baseline_t_index == 0:
            s1 = bitarray('111101')

        if vh_t1_index == 0:
            t1 = bitarray('111000')

        if vth_t2_index == 0:
            u1 = bitarray('101111')

        if vth_e_index == 0:
            v1 = bitarray('010011')

        if baseline_e_index == 0:
            w1 = bitarray('110')

        if fe_delay_index== 0:
            x1 = bitarray('01101')
        elif fe_delay_index== 1:
            x1 = bitarray('01110')
        elif fe_delay_index== 2:
            x1 = bitarray('01111')
        else:
            x1 = bitarray('10000')

        if postamp_gain_t_index == 0:
            y1 = bitarray('00')
        elif postamp_gain_t_index == 1:
            y1 = bitarray('10')
        elif postamp_gain_t_index == 2:
            y1 = bitarray('01')
        else:
            y1 = bitarray('11')

        if postamp_gain_e_index == 0:
            z1 = bitarray('00')
        elif postamp_gain_e_index == 1:
            z1 = bitarray('10')
        elif postamp_gain_e_index == 2:
            z1 = bitarray('01')
        else:
            z1 = bitarray('11')

        if postamp_sh_e_index== 0:
            aa1 = bitarray('00')

        if intg_en_index == 0:
            bb1 = bitarray('0')
        else:
            bb1 = bitarray('1')

        if intg_signal_en_index == 0:
            cc1 = bitarray('0')
        else:
            cc1 = bitarray('1')

        if att_index == 0:
            dd1 = bitarray('000')
        elif att_index == 1:
            dd1 = bitarray('001')
        elif att_index == 2:
            dd1 = bitarray('010')
        elif att_index == 3:
            dd1 = bitarray('011')
        elif att_index == 4:
            dd1 = bitarray('100')
        elif att_index == 5:
            dd1 = bitarray('101')
        elif att_index == 6:
            dd1 = bitarray('110')
        else:
            dd1 = bitarray('111')

        if tdc_current_t_index== 0:
            ee1 = bitarray('0000')

        if tdc_current_e_index == 0:
            ff1 = bitarray('0000')

        if fe_tp_en_index == 0:
            gg1 = bitarray('00')

        if integ_source_sw_index== 0:
            hh1 = bitarray('00')
        elif integ_source_sw_index== 1:
            hh1 = bitarray('01')
        elif integ_source_sw_index== 2:
            hh1 = bitarray('10')
        else:
            hh1 = bitarray('11')

        if t1_hysteresis_index == 0:
            ii1 = bitarray('010')

        if t2_hysteresis_index == 0:
            jj1 = bitarray('010')

        if e_hysteresis_index == 0:
            kk1 = bitarray('010')

        if hysteresis_en_n_index== 0:
            ll1 = bitarray('1')

        return xx,a1,b1,c1,d1,e1,f1,g1,h1,i1,j1,k1,l1,m1,n1,o1,p1,q1,r1,s1,t1,u1,v1,w1,x1,y1,z1,aa1,bb1,cc1,dd1,ee1,ff1,gg1,hh1,ii1,jj1,kk1,ll1

    def n_ch_conf(self):

        if (self.uc.checkBox_all_ch.isChecked() == True):
            self.uc.spinBox_ch_number.setEnabled (False)
        else:
            self.uc.spinBox_ch_number.setEnabled (True)


    def n_ASIC_conf(self):

        if (self.uc.checkBox_all_ASIC.isChecked() == True):
            self.uc.spinBox_ch_number_2.setEnabled (False)
            self.uc.spinBox_ASIC_n.setEnabled (False)
        else:
            self.uc.spinBox_ch_number_2.setEnabled (True)
            self.uc.spinBox_ASIC_n.setEnabled (True)



class AsicConfig():
    """ Constructor.Defines and sets all fields to default values.
        Method

        Parameters

    """
    def __init__(self, upper_class, initial=None, endian="big"):

        self.uc = upper_class
        self.bit_glob = bitarray()
        self.bit_glob_write_conf = bitarray()
        self.bit_ch = bitarray()
        self.bit_ch_write_conf = bitarray()
        self.bit_glob_CRC = bitarray()
        self.bit_ch_CRC = bitarray()

        self.bit_glob.__fields = {
            "tx_nlinks"		: [ n for n in nrange(0, 2) ],
            "tx_ddr"		: [ n for n in nrange(2, 3) ],
            "tx_mode"		: [ n for n in nrange(3, 5) ],
            "debug_mode"		: [ n for n in nrange(5, 6) ],
            "veto_mode"		: [ n for n in nrange(6, 12) ],
            "tdc_clk_div"		: [ n for n in nrange(12, 13) ],
            "r_clk_en"		: [ n for n in nrange(13, 16) ],	# bits 16..17 are ignored
            "stop_ramp_en"		: [ n for n in nrange(18, 20) ],
            "counter_en"		: [ n for n in nrange(20, 21) ],
            "counter_period"	: [ n for n in nrange(21, 24) ],
            "tac_refresh_en"	: [ n for n in nrange(24, 25) ],
            "tac_refresh_period"	: [ n for n in nrange(25, 29) ],
            "data_clk_div"		: [ n for n in nrange(29, 31) ],
            "unused_1"		: [ n for n in range(31, 32) ],
            "fetp_enable"		: [ n for n in range(32, 33) ],
            "input_polarity"	: [ n for n in range(33, 34) ],
            "attenuator_ls"		: [ n for n in range(34, 40) ],
            "v_ref_diff_bias_ig"	: [ n for n in range(40, 46) ],
            "v_cal_ref_ig"		: [ n for n in range(46, 51) ],
            "fe_postamp_t"		: [ n for n in range(51, 56) ],
            "fe_postamp_e"		: [ n for n in range(56, 61) ],
            "v_cal_tp_top"		: [ n for n in range(61, 66) ],
            "v_cal_diff_bias_ig"	: [ n for n in range(66, 71) ],
            "v_att_diff_bias_ig"	: [ n for n in range(71, 77) ],
            "v_integ_ref_ig"	: [ n for n in range(77, 83) ],
            "imirror_bias_top"	: [ n for n in range(83, 88) ],
            "tdc_comp_bias" 	: [ n for n in range(88, 93) ],
            "tdc_i_lsb"		: [ n for n in range(93, 98) ],
            "disc_lsb_t1"		: [ n for n in range(98, 104) ],
            "fe_ib2"		: [134] + [ n for n in range(104, 109) ], # cgate selection is "msb" for ib2
            "vdifffoldcas"		: [ n for n in range(109, 115) ],
            "disc_vcas"		: [ n for n in range(115, 119) ],
            "disc_lsb_e"		: [ n for n in range(119, 125) ],
            "tdc_i_ref" 		: [ n for n in range(125, 130) ],
            "tdc_comp_vcas"		: [ n for n in range(130, 134) ],
            # see fe_ib2 for bit 134
            "main_global_dac"	: [ n for n in range(135, 140) ],
            "fe_ib1"		: [ n for n in range(140, 146) ],
            "disc_ib"		: [ n for n in range(146, 152) ],
            "disc_lsb_t2"		: [ n for n in range(152, 158) ],
            "tdc_tac_vcas_p"	: [ n for n in range(158, 163) ],
            "tdc_tac_vcas_n"	: [ n for n in range(163, 167) ],
            "adebug_out_mode"	: [ n for n in range(167, 169) ],
            "tdc_global_dac"	: [ n for n in range(169, 175) ],
            # 3 bits unused

            "disc_sf_bias"		: [ n for n in range(178, 184) ]
        }

        if initial is not None:
        # We have an initial value, let's just go with it!
        #self[0:169] = bitarray(initial)	;# TOFPET 2a
            self.bit_glob[0:184] = bitarray(initial)	;# TOFPET 2b

        return None

        self.bit_glob_write_conf.__fields = {
            "CRC"		: [ n for n in nrange(0, 8) ],
            "Configuration_register"		: [ n for n in nrange(8, 192) ],
            ""		: [ n for n in nrange(192, 195) ]
        }

        self.bit_glob_CRC.__fields = {
            "Bits for CRC"		: [ n for n in nrange(0, 188) ]
        }

        self.bit_ch.__fields = {
            "trigger_mode_1"	: [ n for n in nrange(0, 2) ],
            "debug_mode"		: [ n for n in nrange(2, 4) ],
            "sync_chain_length"	: [ n for n in nrange(4, 6) ],
            "dead_time"		: [ n for n in nrange(6, 12) ],
            "counter_mode"		: [ n for n in nrange(12, 16) ],
            "tac_max_age"		: [ n for n in nrange(16, 21) ],
            "tac_min_age"		: [ n for n in nrange(21, 26) ],
            "trigger_mode_2_t"	: [ n for n in nrange(26, 28) ],
            "trigger_mode_2_e"	: [ n for n in nrange(28, 31) ],
            "trigger_mode_2_q"	: [ n for n in nrange(31, 33) ],
            "trigger_mode_2_b"	: [ n for n in nrange(33, 36) ],
            "branch_en_eq"		: [ n for n in nrange(36, 37) ],
            "branch_en_t"		: [ n for n in nrange(37, 38) ],
            "qdc_mode"		: [ n for n in nrange(38, 39) ],
            "trigger_b_latched"	: [ n for n in nrange(39, 40) ],
            "min_intg_time"		: [ n for n in nrange(40, 47) ],
            "max_intg_time"		: [ n for n in nrange(47, 54) ],
           "output_en"		: [ n for n in nrange(54, 56) ],
            "qtx2_en"		: [ n for n in nrange(56, 57) ],

            "baseline_t"		: [ n for n in nrange(57, 63) ],
            "vth_t1"		: [ n for n in nrange(63, 69) ],
            "vth_t2"		: [ n for n in nrange(69, 75) ],
            "vth_e"			: [ n for n in nrange(75, 81) ],
            "baseline_e"		: [ n for n in nrange(81, 84) ],
            "fe_delay"		: [84, 88, 87, 85, 86],#[ n for n in nrange(84, 89) ],
            "postamp_gain_t"	: [ n for n in range(89, 91) ],
            "postamp_gain_e"	: [ n for n in range(91, 93) ],
            "postamp_sh_e"		: [ n for n in nrange(93, 95) ],
            "intg_en"		: [ n for n in nrange(95, 96) ],
            "intg_signal_en"	: [ n for n in nrange(96, 97) ],
            "att"			: [ n for n in nrange(97, 100) ],
            "tdc_current_t"		: [ n for n in nrange(100, 104) ],
            "tdc_current_e"		: [ n for n in nrange(104, 108) ],
            "fe_tp_en"		: [ n for n in nrange(108, 110) ],
            "ch63_obuf_msb"		: [ n for n in nrange(110, 111) ],
            #		"tdc_delay"		: [ n for n in nrange(110, 114) ],
            "integ_source_sw"	: [ n for n in nrange(111,112)  ],
            "t1_hysteresis"		: [ n for n in nrange(115, 118) ],
            "t2_hysteresis"		: [ n for n in nrange(118, 121) ],
            "e_hysteresis"		: [ n for n in nrange(121, 124) ],
            "hysteresis_en_n"	: [ n for n in nrange(124, 125) ]

        }

        if initial is not None:
        # We have an initial value, let's just go with it!
            self.bit_ch[0:125] = bitarray(initial)

        return None

        self.bit_glob_write_conf.__fields = {
            "CRC"		: [ n for n in nrange(0, 8) ],
            "Configuration_register"		: [ n for n in nrange(8, 133) ],
            "n_channel"		: [ n for n in nrange(133, 139) ],
            ""		: [ n for n in nrange(139, 144) ]
        }

        self.bit_ch_CRC.__fields = {
            "Bits for CRC"		: [ n for n in nrange(0, 136) ]
        }

    def Config_update_glob(self):

        self.bit_glob[0:184] = bitarray('1100011110010111111101011010110111001011011110001101000100110011101110110110011001011111001110111110111011111000111111111110010111101111111001111111011000010011000100001101000000010110'); # TOFPET 2b

        #print(self.bit_glob[0:184])

        # ASIC parameters to be update
        a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,nn,oo,pp,qq,rr,ss,tt = self.uc.asi_p.selec_glob() #


        self.bit_glob[0:2] = a #self.setValue("tx_nlinks", a)
        self.bit_glob[2:3] = b #self.setValue("tx_ddr", b)
        self.bit_glob[3:5] = c #self.setValue("tx_mode", c)
        self.bit_glob[5:6] = d #self.setValue("debug_mode", d)
        self.bit_glob[6:12] = e #self.setValue("veto_mode", e)
        self.bit_glob[12:13] = f #self.setValue("tdk_clk_div", f)
        self.bit_glob[13:16] = g #self.setValue("r_clk_en ", g)
        self.bit_glob[16:18] = bitarray('00')
        self.bit_glob[18:20] = h #self.setValue("stop_ramp_en", h)
        self.bit_glob[20:21] = i #self.setValue("counter_en", i)
        self.bit_glob[21:24] = j #self.setValue("counter_period", j)
        self.bit_glob[24:25] = k #self.setValue("tac_refresh_en", k)
        self.bit_glob[25:29] = l #self.setValue("tac_refresh_period", l)
        self.bit_glob[29:31] = m #self.setValue("data_clk_div", m)
        self.bit_glob[31:32] = bitarray('0')
        self.bit_glob[32:33] = n #self.setValue("fetp_enabled", n)
        self.bit_glob[33:34] = o #self.setValue("input_polarity", o)
        self.bit_glob[34:40] = p #"attenuator_ls"
        self.bit_glob[40:46] = q #"v_ref_diff_bias_ig"
        self.bit_glob[46:51] = r #"v_cal_ref_ig"
        self.bit_glob[51:56] = s #"fe_postamp_t"
        self.bit_glob[56:61] = t #"fe_postamp_e"
        self.bit_glob[61:66] = u #"v_cal_tp_top"
        self.bit_glob[66:71] = v #"v_cal_diff_bias_ig"
        self.bit_glob[71:77] = w #"v_att_diff_bias_ig"
        self.bit_glob[77:83] = x #"v_integ_ref_ig"
        self.bit_glob[83:88] = y  #"imirror_bias_top"
        self.bit_glob[88:93] = aa  #"tdc_comp_bias"
        self.bit_glob[93:98] = bb  #"tdc_i_lsb"
        self.bit_glob[98:104] = cc #"disc_lsb_t1"
        self.bit_glob[104:109] = dd #"fe_ib2"
        self.bit_glob[109:115] = ee #"vdifffoldcas"
        self.bit_glob[115:119] = ff #"disc_vcas"
        self.bit_glob[119:125] = gg #"disc_lsb_e"
        self.bit_glob[125:130] = hh #"tdc_i_ref"
        self.bit_glob[130:134] = ii #"tdc_comp_vcas"
        self.bit_glob[134:135] = jj #"fe_ib2x2"
        self.bit_glob[135:140] = kk #"main_global_dac"
        self.bit_glob[140:146] = ll #"fe_ib1"
        self.bit_glob[146:152] = mm #"disc_ib"
        self.bit_glob[152:158] = nn #"disc_lsb_t2"
        self.bit_glob[158:163] = oo #"tdc_tac_vcas_p"
        self.bit_glob[163:167] = pp #"tdc_tac_vcas_n"
        self.bit_glob[167:169] = qq #"adebug_out_mode"
        self.bit_glob[169:174] = rr #"tdc_global_dac"
        self.bit_glob[174:175] = ss #"adebug_out_mode"
        self.bit_glob[175:178] = bitarray('000')
        self.bit_glob[178:184] = tt #"disc_sf_bias

    # print(self.bit_glob[0:2])
    # print(self.bit_glob[2:3])
    # print(self.bit_glob[3:5])
    # print(self.bit_glob[5:6])
    # print(self.bit_glob[6:12])
    # print(self.bit_glob[12:13])
    # print(self.bit_glob[13:16])
    # print(self.bit_glob[18:20])
    # print(self.bit_glob[20:21])
    # print(self.bit_glob[21:24])
    # print(self.bit_glob[24:25])
    # print(self.bit_glob[25:29])
    # print(self.bit_glob[29:31])
    # print(self.bit_glob[31:32])
    # print(self.bit_glob[32:33])
    # print(self.bit_glob[33:34])
    # print(self.bit_glob[34:40])
    # print(self.bit_glob[40:46])
    # print(self.bit_glob[46:51])
    # print(self.bit_glob[51:56])
    # print(self.bit_glob[56:61])
    # print(self.bit_glob[61:66])
    # print(self.bit_glob[66:71])
    # print(self.bit_glob[71:77])
    # print(self.bit_glob[77:83])
    # print(self.bit_glob[83:88])
    # print(self.bit_glob[88:93])
    # print(self.bit_glob[93:98])
    # print(self.bit_glob[98:104])
    # print(self.bit_glob[104:109])
    # print(self.bit_glob[109:115])
    # print(self.bit_glob[115:119])
    # print(self.bit_glob[119:125])
    # print(self.bit_glob[125:130])
    # print(self.bit_glob[130:134])
    # print(self.bit_glob[134:135])
    # print(self.bit_glob[135:140])
    # print(self.bit_glob[140:146])
    # print(self.bit_glob[146:152])
    # print(self.bit_glob[152:158])
    # print(self.bit_glob[158:163])
    # print(self.bit_glob[163:167])
    # print(self.bit_glob[167:169])
    # print(self.bit_glob[169:174])
    # print(self.bit_glob[174:175])
    # print(self.bit_glob[175:178])
    # print(self.bit_glob[178:184])

        #print(self.bit_glob[0:184])

        self.bit_glob_CRC[0:184] = self.bit_glob[0:184]
        self.bit_glob_CRC[184:188] = bitarray('1000')
        _CRC = self.bit_glob_CRC[0:188]

        print(_CRC)


        #GLOBAL CONFIGURATION WRITE COMMAND

        if self.uc.checkBox_all_ASIC.isChecked() == False:

            self.bit_glob_write_conf[0:8] = bitarray(crc_8(_CRC)) #CRC
            self.bit_glob_write_conf[8:192] = self.bit_glob[0:184]
            self.bit_glob_write_conf[192:196] = bitarray('1000')

        else:

            self.bit_glob_write_conf[0:8] = bitarray(crc_8(_CRC)) #CRC
            self.bit_glob_write_conf[8:192] = self.bit_glob[0:184]
            self.bit_glob_write_conf[192:196] = bitarray('1000')


        print(self.bit_glob_write_conf[0:196])

        self.uc.c.Config_reg_glob_ASIC()
        self.uc.b_buttons.f_save_globConfig()

    def Config_update_ch(self):

        # Specify default value
        self.bit_ch[0:125] = bitarray('10100100100000100000000001101100000000001111010011101111111000111101000000111000001110111101010100101010111110000000000000000')

        print(self.bit_ch[0:125])

        # ASIC channel parameters to be update
        xx,a1,b1,c1,d1,e1,f1,g1,h1,i1,j1,k1,l1,m1,n1,o1,p1,q1,r1,s1,t1,u1,v1,w1,x1,y1,z1,aa1,bb1,cc1,dd1,ee1,ff1,gg1,hh1,ii1,jj1,kk1,ll1 = self.uc.asi_p.selec_ch() #

        self.bit_ch[0:2] = xx # self.setValue("set_triggwer_mode1", a)
        self.bit_ch[2:3] = a1 # self.setValue("debug_mode", b)
        self.bit_ch[4:6] = b1 # "sync_chain_length"
        self.bit_ch[6:12] = c1 # "dead_time"
        self.bit_ch[12:16] = d1 #"counter_mode"
        self.bit_ch[16:21] = e1 # tac_max_age
        self.bit_ch[21:26] = f1 # "tac_min_age"
        #self.bit_ch[26:28] = g1 # "trigger_mode_2_t"
        self.bit_ch[28:31] = h1 # "trigger_mode_2_e"
        self.bit_ch[31:33] = i1 # "trigger_mode_2_q"
        self.bit_ch[33:36] = j1 # "trigger_mode_2_b"
        self.bit_ch[36:37] = k1 # "branch_en_eq"
        self.bit_ch[37:38] = l1 # "branch_en_t"
        self.bit_ch[38:39] = m1 # "qdc_mode"
        self.bit_ch[39:40] = n1 # "trigger_b_latched"
        self.bit_ch[40:47] = o1 # "min_intg_time"
        self.bit_ch[47:54] = p1 # "max_intg_time"
        self.bit_ch[54:56] = q1 # output_en"
        self.bit_ch[56:57] = r1 # "qtx2_en"
        self.bit_ch[57:63] = s1 # "baseline_t"
        self.bit_ch[63:69] = t1 # "vth_t1"
        self.bit_ch[69:75] = u1 # "vth_t2"
        self.bit_ch[75:81] = v1 # "vth_e"
        self.bit_ch[81:84] = w1 # "baseline_e"
        #self.bit_ch[84][88][87][85][86] = x #"fe_delay"		#[ n for n in nrange(84, 89) ],
        self.bit_ch[84] = bitarray('0')
        self.bit_ch[88] = bitarray('1')
        self.bit_ch[87] = bitarray('1')
        self.bit_ch[85] = bitarray('1')
        self.bit_ch[86] = bitarray('0')
        self.bit_ch[89:91] = y1 # "postamp_gain_t"
        self.bit_ch[91:93] = z1 # "postamp_gain_e"
        self.bit_ch[93:95] = aa1 # "postamp_sh_e"
        self.bit_ch[95:96] = bb1 # "intg_en"
        self.bit_ch[96:97] = cc1 # "intg_signal_en"
        self.bit_ch[97:100] = dd1 # "att"
        self.bit_ch[100:104] = ee1 # "tdc_current_t"
        self.bit_ch[104:108] = ff1 # "tdc_current_e"
        self.bit_ch[108:110] = gg1 # "fe_tp_en"
        self.bit_ch[110:111] = bitarray('1') # "n/u
        self.bit_ch[111:112] = hh1 # "integ_source_sw"
        self.bit_ch[115:118] = ii1 # "t1_hysteresis"
        self.bit_ch[118:121] = jj1 # "t2_hysteresis"
        self.bit_ch[121:124] = kk1 # "e_hysteresis"
        self.bit_ch[124:125] = ll1 # "hysteresis_en_n"

        print(self.bit_ch[0:125])

        #CHANNEL CONFIGURATION WRITE COMMAND

        if self.uc.checkBox_all_ch.isChecked() == True:

            self.bit_ch_CRC[0:125] = self.bit_ch[0:125]
            self.bit_ch_CRC[125:130] = bitarray('1000')
            self.bit_ch_CRC[130:136] = bitarray('00000')

            #print(self.bit_glob_write_conf[0:196])
            #print(self.bit_glob_write_conf[8:196])
            _CRC = self.bit_ch_CRC[0:136]
            print(_CRC)

            self.bit_ch_write_conf[0:8] = bitarray(crc_8(_CRC)) #CRC
            self.bit_ch_write_conf[8:133] = self.bit_ch[0:125]
            self.bit_ch_write_conf[133:139] = bitarray('1000') #?
            self.bit_ch_write_conf[139:144] = bitarray('00000')

            print(self.bit_ch_write_conf[0:144])

        else:

            n_ch =self.uc.b_buttons.int_v(self.uc.spinBox_ch_number.value())

            if n_ch >=0 and n_ch <=1:
                n_ch_bin = str(bin(n_ch)).replace("0b","")
                n_ch_bin = '00000' + n_ch_bin
                print(n_ch_bin)
            elif n_ch >=2 and n_ch <=3:
                n_ch_bin = str(bin(n_ch)).replace("0b","")
                n_ch_bin = '0000' + n_ch_bin
                print(n_ch_bin)
            elif n_ch >=4 and n_ch <=7:
                n_ch_bin = str(bin(n_ch)).replace("0b","")
                n_ch_bin = '000' + n_ch_bin
                print(n_ch_bin)
            elif n_ch >=8 and n_ch <=15:
                n_ch_bin = str(bin(n_ch)).replace("0b","")
                n_ch_bin = '00' + n_ch_bin
                print(n_ch_bin)
            elif n_ch >=16 and n_ch <=31:
                n_ch_bin = str(bin(n_ch)).replace("0b","")
                n_ch_bin = '0' + n_ch_bin
                print(n_ch_bin)
            elif n_ch >=32 and n_ch <=63:
                n_ch_bin = str(bin(n_ch)).replace("0b","")
                print(n_ch_bin)

            self.bit_ch_CRC[0:125] = self.bit_ch[0:125]
            self.bit_ch_CRC[125:130] = bitarray(n_ch_bin)
            self.bit_ch_CRC[130:136] = bitarray('00000')

            _CRC = self.bit_ch_CRC[0:136]
            print(_CRC)

            self.bit_ch_write_conf[0:8] = bitarray(crc_8(_CRC)) #CRC
            self.bit_ch_write_conf[8:133] = self.bit_ch[0:125]
            self.bit_ch_write_conf[133:139] = bitarray(n_ch_bin)
            self.bit_ch_write_conf[139:144] = bitarray('00000')

            print(self.bit_ch_write_conf[0:144])


        self.uc.c.Config_reg_ch_ASIC()
        self.uc.b_buttons.f_save_chConfig()
